/*JavaScript que maneja los canvas de las páginas*/

//Declaraciones...
var canvas; /*variables para manejar*/
var contexto; /*el canvas*/

//variables que guardarán el código html y js para mostrar al usuario
var codigo_html = "";
var codigo_js = "";

//variables necesarias para mostrar el código generado javascript
var TextoAct="";
var n=-1;

//variables necesarias para mostrar código en pantalla ( el js y el html)
var velocidad=10; //velocidad con que se muestra
var micontrol;  //control con el cual se va a mostrar el código, cambiará depende donde queremos mostrar...

//variables para guardar las dimensiones ingresadas por el usuario (aquí puede cambiar por un arrreglo, pero tendríamos que meter los controles en un form con name)
var valorx = 0;
var valory = 0;
var valorancho = 0;
var valoralto = 0;

//la ocupamos en 'ejemplos y ejercicios.html'
var valornumrectangulos = 0;

//al cargar la página, cargamos el canvas
window.onload = function(){
	
	/*anda a escribir el código html necesario para utilizar el canvas (éste no cambia) por eso lo imprimimos desde el principio*/
	codigo_html = "<html>\n<head><title>Canvas1</title>\n</head>\n<body>\n<canvas id='canvas1' width='800' height='400'>\nTu navegador no soporta el Canvas...\n</body>\n</canvas>\n</html>"
	guardaCodigo(codigo_html, document.getElementById('codigohtml')); /*mandamos a escribirlo*/

	/*inicializamos contador*/
	canvas = null;
	contexto = null;

}//fin onload

function creaCanvas(idCanvas){
	canvas = null;
	contexto = null;

	contexto = cargaContextoCanvas(idCanvas);

	if(contexto){
		alert("Se ha creado correctamente el canvas --> "+idCanvas);
	}else{
		alert("No se pudo crear el canvas --> "+idCanvas)
	}
	
}//fin creaCanvas()

//función aux de CreaCanvas() para saber si creamos el canvas correctamente
function cargaContextoCanvas(idCanvas){
	canvas = document.getElementById(idCanvas);
	if(canvas && canvas.getContext){
		var contexto = canvas.getContext('2d');
		if(contexto){
			return contexto;
		}else{
			//alert("El canvas no se pudo crear :C");
		}
	}
	return FALSE;
}//fin cargaContextoCanvas()

//función que reinicia(pone en blanco contenido) todos los parámetros recibidos como parámetros
//function reiniciaControles(){

	/*Así llamamos a ésta función*/
	//reiniciaControles(document.getElementById('posicioninicialx'));
	
	//los sig comentarios fueron para entender cómo funciona éste método
	//arguments[0].value = ""; //borra el primer control enviado como parámetro
	//alert("msn recibido -> "+arguments[arguments.length-1]); //imprime el valor del último valor enviado
	//arguments[0].value = arguments[arguments.length] + "2"; //

	/*Ponemos en blanco todos los contorles recibidos*/
//	for(i=0; i<arguments.length; i++){
//		arguments[i].value = "";
//	}

//función que reinicia todos los inputs, excepto si son de tipo botón
//sirve cómo función auxiliar para el reiniciar
function reiniciaControles(){

	alert("Vamos a reiniciar todos los inputs...");
	var inputs = document.getElementsByTagName('input');

	 for(var i=0;i<inputs.length;i++) {
	 	if(inputs[i].type != "button")
	 	{
	 		inputs[i].value="";
	 	}
	   
	}
}//fin reiniciaControles()

//función que usamos en el archivo 'miprimercanvas.html' para dibujar mi primer rectángulo con canvas
function dibujaRectangulo()
{
	LimpiarCanvas(); /*borra el contenido actual del canvas*/
	
	/*obtiene los valores ingresados por el usuario*/
	valorX = document.getElementById("X").value; valorx = valorX;
	valorY = document.getElementById("Y").value; valory = valorY;
	valorAncho = document.getElementById("Ancho").value; valorancho = valorAncho;
	valorAlto = document.getElementById("Alto").value; valoralto = valorAlto;

	/*valida los valores del usuario*/
	if(isNaN(valorX) || isNaN(valorY) || isNaN(valorAncho) || isNaN(valorAlto)) //si hay algún valor que ingresó el usuario y no es número
	{
		//checamos cuál es y le asignamos 0
		if(isNaN(valorX) ) { //si es un NoNumero
			valorX = 0;
			valorx = valorX;
		}
		if(isNaN(valorY) ) { //si es un NoNumero
			valorY = 0;
			valory = valorY;
		}
		if(isNaN(valorAncho) ) { //si es un NoNumero
			valorAncho = 0;
			valorancho = valorAncho;
		}
		if(isNaN(valorAlto) ) { //si es un NoNumero
			valorAlto = 0;
			valoralto = valorAlto;
		}

		document.getElementById('mensajeresultado').innerHTML = "¡Debes ingresar sólo valores numéricos!";
	}else{
		document.getElementById('mensajeresultado').innerHTML = "";
	}//si no, no los toques y que sigan su flujo
	


	/*3. dibujamos y pintamos el rectangulo con los valores del usuario*/
	/*x,y,ancho,alto del rectangulo*/
	contexto.fillStyle = '#f16529';
	contexto.fillRect(valorX,valorY,valorAncho, valorAlto);

}//fin dibujaRectangulo()

//función que usamos en el archivo 'ejemplos y ejercicios.html'
function dibujaRectanguloConBucle()
{
	
	LimpiarCanvas(); /*borra el contenido actual del canvas*/
	
	/*obtiene los valores ingresados por el usuario*/
	valornumrectangulos = document.getElementById("numrectangulos").value;
	valorX = document.getElementById("X").value; valorx = valorX;
	valorY = document.getElementById("Y").value; valory = valorY;
	valorAncho = document.getElementById("Ancho").value; valorancho = valorAncho;
	valorAlto = document.getElementById("Alto").value; valoralto = valorAlto;

	/*valida los valores del usuario*/
	if(isNaN(valornumrectangulos) || isNaN(valorX) || isNaN(valorY) || isNaN(valorAncho) || isNaN(valorAlto)) //si hay algún valor que ingresó el usuario y no es número
	{
		if(isNaN(numrectangulos)){
			document.getElementById('mensajeresultado').innerHTML = "¡Ingresa sólo números!";
		}return;

		if(isNan)
		//checamos cuál es y le asignamos 0
		if(isNaN(valorX) ) { //si es un NoNumero
			valorX = 0;
			valorx = valorX;
		}
		if(isNaN(valorY) ) { //si es un NoNumero
			valorY = 0;
			valory = valorY;
		}
		if(isNaN(valorAncho) ) { //si es un NoNumero
			valorAncho = 0;
			valorancho = valorAncho;
		}
		if(isNaN(valorAlto) ) { //si es un NoNumero
			valorAlto = 0;
			valoralto = valorAlto;
		}

		document.getElementById('mensajeresultado').innerHTML = "¡Debes ingresar sólo valores numéricos!";
	}else{
		document.getElementById('mensajeresultado').innerHTML = "";
	}//si no, no los toques y que sigan su flujo
	


	/*3. dibujamos y pintamos el rectangulo con los valores del usuario*/
	/*x,y,ancho,alto del rectangulo*/
	//contexto.fillStyle = '#f16529';
	//contexto.fillRect(valorX,valorY,valorAncho, valorAlto);
	contexto.strokeStyle = '#ff9933';//cambio el color de la línea de borde del rectángulo

	contexto.fillStyle = '#cc0000'; //color de relleno
	for (i=0;i<(10*valornumrectangulos);i+=10){
		// fillRect(x,y,width,height) dibuja un rectángulo relleno de color
		contexto.fillRect(i,i,valorancho,valoralto);
	}

	//visualizamos el código en el control
	codigo_js = "var canvas = document.getElementById('canvas');\ncontexto = canvas.getContext('2d');\ncontexto.fillStyle = '#f16529';\ncontexto.fillRect("+valorx+","+valory+","+valorancho+","+valoralto+");";
	//mandamos -> código que queremos se muestre , control en el que queremos se muestre PERO SÓLO SE PUEDE CON UNO
	//guardaCodigo(codigohtml, document.getElementById('codigohtml'));
	

}//fin dibujaRectanguloConBucle()

//función que usamos en el archivo 'ejemplos y ejercicios.html'
function dibujaCamino1()
{

	LimpiarCanvas();

	//alert("Estoy en dibujaCamino1()");

	//variables de ésta función
	var posicioninicialx;
	var posicioninicialy;
	var linea1x;
	var linea1y;
	var linea2x;
	var linea2y;
	var linea3x;
	var linea3y;

	//obtenemos los datos ingresados por el usuario
	posicioninicialx = document.getElementById('posicioninicialx').value;
	posicioninicialy = document.getElementById('posicioninicialy').value;
	linea1x = document.getElementById('linea1x').value;
	linea1y = document.getElementById('linea1y').value;
	linea2x = document.getElementById('linea2x').value;
	linea2y = document.getElementById('linea2y').value;
	linea3x = document.getElementById('linea3x').value;
	linea3y = document.getElementById('linea3y').value;

	/*valida los valores del usuario*/
		if(isNaN(posicioninicialx) || isNaN(posicioninicialy) || isNaN(linea1x) || isNaN(linea1y) || isNaN(linea2x) || isNaN(linea2y) || isNaN(linea3x) || isNaN(linea2y)) //si hay algún valor que ingresó el usuario y no es número
		{
			if(isNaN(posicioninicialx)){
				posicioninicialx = 0;
			}

			//checamos cuál es y le asignamos 0
			if(isNaN(posicioninicialy) ) { //si es un NoNumero
				posicioninicialy = 0;
			}
			if(isNaN(linea1x) ) { //si es un NoNumero
				linea1x = 0;
			}
			if(isNaN(linea1y) ) { //si es un NoNumero
				linea1y = 0;
			}
			if(isNaN(linea2x) ) { //si es un NoNumero
				linea2x = 0;
			}

			if(isNaN(linea2y) ) { //si es un NoNumero
				linea2xy = 0;
			}

			if(isNaN(linea3x) ) { //si es un NoNumero
				linea3x = 0;
			}

			if(isNaN(linea3y) ) { //si es un NoNumero
				linea3y = 0;
			}

			document.getElementById('mensajeresultado').innerHTML = "¡Debes ingresar sólo valores numéricos!";
		}else{
			document.getElementById('mensajeresultado').innerHTML = "";
		}//si no, no los toques y que sigan su flujo

	//console.log("pos inicial -> (", posicioninicialx + " , "+posicioninicialy+", "+linea1x+", "+linea1y+", "+linea2x+","+linea2y+","+linea3x+","+linea3y+")"); // <-- Nos sirvió sólo para comprobar que llegaran los datos...
	contexto.strokeStyle = '#ff9933';//cambio el color de la línea de borde del rectángulo

	contexto.fillStyle = '#cc0000'; //color de relleno

	contexto.beginPath();
	contexto.moveTo(posicioninicialx,posicioninicialy); //posición(inicio, fin) -> 50,5
	contexto.lineTo(linea1x,linea1y); //linea(inicio, fin) 						->75,65
	contexto.lineTo(linea2x,linea2y); //										->50,125
	contexto.lineTo(linea3x,linea3y);//											->25,65
	contexto.fill();
	

}//fin dibujaCamino1()





/*Aquí tratamos de hacerlo más genérico...
function dibujaRectangulo()
{
	alert("soi la otra...");
	Reiniciar(); 
	
	//aquí podemos hacer un for
	valorX = arguments[0];
	valorY = arguments[1];
	valorAncho = arguments[2];
	valorAlto = arguments[3];

	contexto.fillRect(valorX,valorY,valorAncho, valorAlto);
}
*/

//función que borra el contenido del canvas actual
function LimpiarCanvas(){
	contexto.clearRect(0, 0, canvas.width, canvas.height);
	/*reiniciamos controles*/
}

/*función que borra el contenido del canvas y limpia los controles*/
/*lo usamos en las páginas -> miprimercanvas.html y ejemplosyejercicios.html(sólo para el primer ejemplo) */
/*Estamos buscando cómo hacerlo más genérico*/
function Reiniciar(){

	contexto.clearRect(0, 0, canvas.width, canvas.height);
	
	/*reiniciamos controles*/
	document.getElementById("X").value = "";
	document.getElementById("Y").value = "";
	document.getElementById("Ancho").value = "";
	document.getElementById("Alto").value = "";

	/*mandamos el focus al primer input*/
	document.getElementById("X").focus();

	/*reiniciamos nuestras variables auxiliares*/
	valorx = 0;
	valory = 0;
	valorancho = 0;
	valoralto = 0;


}//fin Reiniciar()

/*Aquí tratamos de hacerlo más genérico...
function dibujaRectangulo()
{
	alert("soi la otra...");
	Reiniciar(); 
	
	//aquí podemos hacer un for
	valorX = arguments[0];
	valorY = arguments[1];
	valorAncho = arguments[2];
	valorAlto = arguments[3];

	contexto.fillRect(valorX,valorY,valorAncho, valorAlto);
}
*/

function Reiniciar2(){
	
}

//recibe formulario con todos los controles que tiene y los REINICIA
function ReiniciarMejorado(idFormulario){
	//...
}
/*......................Funciones para escribir código en el textarea........................*/

//Ésta función se llamará cuando queramos visualizar los códigos necesarios para hacer la chamba del canvas
function actualizaCodigoJS()
{

	codigo_js = "var canvas = document.getElementById('canvas1');\ncontexto = canvas.getContext('2d');\ncontexto.fillStyle = '#f16529';\ncontexto.fillRect("+valorx+","+valory+","+valorancho+","+valoralto+");";
	//mandamos -> código que queremos se muestre , control en el que queremos se muestre PERO SÓLO SE PUEDE CON UNO
	//guardaCodigo(codigohtml, document.getElementById('codigohtml'));
	guardaCodigo(codigo_js, document.getElementById('codigojs'));
	
}//fin actualizaCodigoJS()

function actualizaCodigoJS2(){
	codigo_js2 = "var canvas = document.getElementById('canvas1');\ncontexto = canvas.getContext('2d');\ncontexto.strokeStyle = '#ff9933';\ncontexto.fillStyle = '#cc0000'; //color de relleno\nfor (i=0;i<(10*"+valornumrectangulos+");i+=10){\ncontexto.fillRect(i,i,"+valorancho+","+valoralto+");//Aquí usamos i,i para posX y posY para que se puedan visualizar los diferentes rectángulos...Tú puedes jugar con dichos valores\n}\n";
	//mandamos -> código que queremos se muestre , control en el que queremos se muestre PERO SÓLO SE PUEDE CON UNO
	//guardaCodigo(codigohtml, document.getElementById('codigohtml'));
	guardaCodigo(codigo_js2, document.getElementById('codigojs'));
}//fin actualizaCodigoJS2()


function guardaCodigo(codigo, control) //méte las letras al arreglo letra por letra
{ 
	
micontrol = null;
micontrol = control;
//alert('vamos a escribir en -> '+control);
//console.log("vamos a guardar -> "+codigo);
//Declaramos un arreglo donde meteremos el texto a animar
 letras=new Array();

 //Texto que se va a animar
 var texto=codigo;
//console.log("guardamos -> "+texto);

 //Metemos letra por letra la cadena que se va a animar al array
 for(i=0;i<texto.length;i++)
 {
    letras[i]=texto.charAt(i);	//asigna a letras uno a uno los caracteres de texto
    //console.log("guardamos en el arreglo -> "+letras[i]);
 }

	TextoAct = "";
	n = -1;
	visualizaCodigo();

}//fin animar()

//función que anima el texto

function visualizaCodigo()
{
  //obtenemos 
  n++;
  TextoAct += letras[n];
  /*alert("Vamos a mostrar: "+TextoAct);*/
  micontrol.value=TextoAct;

  if(n==letras.length-1) /*si llegamos al final*/
  {
  	/*n=-1;*/
  	/*TextoAct="";*/
  	return; /*con éste return paramos la animación al terminar de escribir*/
  }

  setTimeout("visualizaCodigo()",velocidad); //manda a ejecutar otra vez en ése tiempo
}//fin mueveLetras()

