function insertarVideo() {
    var contenido = document.getElementById("contenido").value;
    limpiarVideo();
    var div = document.getElementById("video");
    div.innerHTML = contenido;
}

function limpiarVideo() {
    var div = document.getElementById("video");
    div.innerHTML = "";
}

function insertarAudio() {
    var contenido = document.getElementById("contenido").value;
    limpiarAudio();
    var div = document.getElementById("audio");
    div.innerHTML = contenido;
}

function limpiarAudio() {
    var div = document.getElementById("audio");
    div.innerHTML = "";
}